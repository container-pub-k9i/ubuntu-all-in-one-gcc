FROM registry.gitlab.com/container-pub-k9i/debug-ubuntu-all-in-one:master

#
# NOTE: ENV affects only in container or RUN command (not in Dockerfile)
#
ENV IS_DOCKER=1

RUN set -eux; \
    sudo -H apt-get update

RUN set -eux; \
    sudo -H apt-get install -y --no-install-recommends \
    gcc python3-dev

RUN sudo -H pip3 install --upgrade pip
